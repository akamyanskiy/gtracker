from django.conf.urls import patterns, include, url
from track.views import report_120, report_121, report_develop, report_laika, synchronize

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    ('^$', report_laika),
    ('^report1/$', report_laika),
    ('^report2/$', report_develop),
    ('^report3/$', report_120),
    ('^report4/$', report_121),
    ('^sync/$' , synchronize),
    # Examples:
    # url(r'^$', 'gtracker.views.home', name='home'),
    # url(r'^gtracker/', include('gtracker.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
