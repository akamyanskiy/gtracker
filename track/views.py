# Create your views here.

from django.shortcuts import render_to_response
from models import Issue
import datetime
from conf import username, password, org_name, repo_name
from github import Github


# u'Bug', u'High', u'Admin', u'Application', u'Branch:develop', u'Laika'
def _query_develop():
    set_high = Issue.objects.filter(labels__contains='Bug').\
                        filter(status="open").\
                        filter(labels__contains='Branch:develop').\
                        filter(labels__contains='High').\
                        exclude(labels__contains='Laika').\
                        order_by('-number')

    set_normal = Issue.objects.filter(labels__contains='Bug').\
                        filter(status="open").\
                        filter(labels__contains='Branch:develop').\
                        filter(labels__contains='Normal').\
                        exclude(labels__contains='Laika').\
                        order_by('-number')


    set_low = Issue.objects.filter(labels__contains='Bug').\
                        filter(status="open").\
                        filter(labels__contains='Branch:develop').\
                        filter(labels__contains='Low').\
                        exclude(labels__contains='Laika').\
                        order_by('-number')
    return set_high, set_normal, set_low

def _query_laika():
    set_high_laika = Issue.objects.filter(labels__contains='Bug').\
        filter(status="open").\
        filter(labels__contains='Branch:develop').\
        filter(labels__contains='High').\
        filter(labels__contains='Laika').\
        exclude(labels__contains='NX').\
        order_by('-number')


    set_normal_laika = Issue.objects.filter(labels__contains='Bug').\
        filter(status="open").\
        filter(labels__contains='Branch:develop').\
        filter(labels__contains='Normal').\
        filter(labels__contains='Laika').\
        exclude(labels__contains='NX').\
        order_by('-number')

    set_low_laika = Issue.objects.filter(labels__contains='Bug').\
        filter(status="open").\
        filter(labels__contains='Branch:develop').\
        filter(labels__contains='Low').\
        filter(labels__contains='Laika').\
        exclude(labels__contains='NX').\
        order_by('-number')
    return set_high_laika, set_normal_laika, set_low_laika

def _query_120():
    set_high_120 = Issue.objects.filter(labels__contains='Bug').\
        filter(status="open").\
        filter(labels__contains='Branch 1.2.0').\
        filter(labels__contains='High').\
        order_by('-number')

    set_normal_120 = Issue.objects.filter(labels__contains='Bug').\
        filter(status="open").\
        filter(labels__contains='Branch 1.2.0').\
        filter(labels__contains='Normal').\
        order_by('-number')


    set_low_120 = Issue.objects.filter(labels__contains='Bug').\
        filter(status="open").\
        filter(labels__contains='Branch 1.2.0').\
        filter(labels__contains='Low').\
        order_by('-number')
    return set_high_120, set_normal_120, set_low_120

def _query_121():
    set_high_121 = Issue.objects.filter(labels__contains='Bug').\
    filter(status="open").\
    filter(labels__contains='Branch 1.2.1').\
    filter(labels__contains='High').\
    order_by('-number')

    set_normal_121 = Issue.objects.filter(labels__contains='Bug').\
    filter(status="open").\
    filter(labels__contains='Branch 1.2.1').\
    filter(labels__contains='Normal').\
    order_by('-number')


    set_low_121 = Issue.objects.filter(labels__contains='Bug').\
    filter(status="open").\
    filter(labels__contains='Branch 1.2.1').\
    filter(labels__contains='Low').\
    order_by('-number')
    return set_high_121, set_normal_121, set_low_121


def report_develop(request):
    now = datetime.datetime.now()
    set_high, set_normal, set_low = _query_develop()
    return render_to_response('report_base.html', {'current_date': now,
                                                   'set_of_all': (('High',set_high,'#ff9999'),
                                                                  ('Normal',set_normal,'#66CCFF'),
                                                                  ('Low',set_low,'#FFFF66')),
                                                   'title_text': "Develop branch, nuxeo's backend issues; "
                                                   })

def report_laika(request):
    now = datetime.datetime.now()
    set_high_laika, set_normal_laika, set_low_laika = _query_laika()
    return render_to_response('report_base.html', {'current_date': now,
                                                   'set_of_all': (('High',set_high_laika,'#ff9999'),
                                                                  ('Normal',set_normal_laika,'#66CCFF'),
                                                                  ('Low',set_low_laika,'#FFFF66')),
                                                   'title_text': "Develop branch; Laika's backend issues; "
                                                   })
def report_120(request):
    now = datetime.datetime.now()
    set_high_120, set_normal_120, set_low_120 = _query_120()
    return render_to_response('report_base.html', {'current_date': now,
                                                   'set_of_all': (('High',set_high_120,'#ff9999'),
                                                                  ('Normal',set_normal_120,'#66CCFF'),
                                                                  ('Low',set_low_120,'#FFFF66')),
                                                   'title_text':"release-1.2.0 branch issues; "
                                                   })

def report_121(request):
    now = datetime.datetime.now()
    set_high_121, set_normal_121, set_low_121 = _query_121()
    return render_to_response('report_base.html', {'current_date': now,
                                                   'set_of_all': (('High',set_high_121,'#ff9999'),
                                                                  ('Normal',set_normal_121,'#66CCFF'),
                                                                  ('Low',set_low_121,'#FFFF66')),
                                                   'title_text':"release-1.2.1 branch issues; "
    })


def _get_labels(issue):
    labels = []
    if issue.labels:
        for label in issue.labels:
            labels.append(label.name)
    return labels

def synchronize(request):

    g = Github( username, password )

    organization = g.get_organization(org_name)
    repo = organization.get_repo(repo_name)
    set_open = repo.get_issues(state='open')
    set_closed =  repo.get_issues(state='closed') # or closed

    for issues in (set_open, set_closed):
        i=0
        while 1:
            page = issues.get_page(i)
            if page:
                for issue in page:
                    # process fill db
                    if issue.assignee:
                        assigned = issue.assignee.login
                    else:
                        assigned = 'n/a'
                    if issue.user:
                        creator = issue.user.login
                    else:
                        creator = 'n/a'

                    if issue.milestone:
                        milestone = issue.milestone.title
                    else:
                        milestone = ''
                    obj = {'number': issue.number,
                           'title': issue.title,
                           'status': issue.state,
                           'creator': creator,
                           'assigned': assigned,
                           'created': issue.created_at.date(),
                           'url': issue.url,
                           'milestone': milestone,
                           'labels': _get_labels(issue),
                           }
                    if not Issue.objects.filter(number=issue.number):
                        Issue.objects.create(**obj)
                    else:
                        rec = Issue.objects.get(number=issue.number)
                        rec.number = obj['number']
                        rec.status = obj['status']
                        rec.title = obj['title']
                        rec.creator = obj['creator']
                        rec.assigned = obj['assigned']
                        rec.created = obj['created']
                        rec.url = obj['url']
                        rec.milestone = obj['milestone']
                        rec.labels = obj['labels']
                        rec.save()
                i+=1
            else:
                break
    return render_to_response('sync_report.html', {'total': Issue.objects.count()})