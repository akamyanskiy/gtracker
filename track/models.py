from django.db import models

# Create your models here.
class Issue(models.Model):
    number = models.IntegerField()
    status = models.CharField(max_length=10)
    title = models.TextField(max_length=2048)
    creator = models.CharField(max_length=255)
    assigned = models.CharField(max_length=255)
    created = models.DateTimeField()
    labels = models.CharField(max_length=255)
    url = models.URLField()
    milestone = models.CharField(max_length=255)

    def __unicode__(self):
        return "{0}, {1}, {2}".format(self.number, self.status, self.title[:90])

